package com.cajapaita.pe.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cajapaita.pe.model.Venta;
import com.cajapaita.pe.service.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private IVentaService service;
	
	@GetMapping(produces="application/json")
	public List<Venta>listar(){
		return service.listar();
	}
	
	@GetMapping(value="/{id}",produces="application/json")
	public Venta listarPorId(@PathVariable("id") Integer id) {
		return service.listarid(id);
	}
	
	@PostMapping(produces="application/json",consumes="application/json")
	public Venta registrar(@RequestBody Venta Venta) {
		return service.registrar(Venta);
	}
	

	

}
