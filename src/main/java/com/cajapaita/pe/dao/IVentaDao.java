package com.cajapaita.pe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cajapaita.pe.model.Venta;

public interface IVentaDao extends JpaRepository<Venta, Integer> {

}
