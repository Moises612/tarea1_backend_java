package com.cajapaita.pe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cajapaita.pe.model.Producto;

public interface IProductoDao extends JpaRepository<Producto, Integer> {

}
