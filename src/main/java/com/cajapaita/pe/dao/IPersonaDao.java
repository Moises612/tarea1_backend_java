package com.cajapaita.pe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cajapaita.pe.model.Persona;


public interface IPersonaDao extends JpaRepository<Persona, Integer> {

}
