package com.cajapaita.pe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TareaBackend1Application {

	public static void main(String[] args) {
		SpringApplication.run(TareaBackend1Application.class, args);
	}
}
