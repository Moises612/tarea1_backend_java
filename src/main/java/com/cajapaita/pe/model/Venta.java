package com.cajapaita.pe.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name = "ventas")
public class Venta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer cod_venta;
	
	@JsonSerialize(using = ToStringSerializer.class)
	@Temporal(TemporalType.DATE)
	private Date fecha;
	
	@ManyToOne
	@JoinColumn(name="cod_persona", nullable=false)
	private Persona persona;
	
	private Double montototal;

	@OneToMany(mappedBy = "venta",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<DetalleVentas> items;

	public Integer getCod_venta() {
		return cod_venta;
	}

	public void setCod_venta(Integer cod_venta) {
		this.cod_venta = cod_venta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Double getMontototal() {
		return montototal;
	}

	public void setMontototal(Double montototal) {
		this.montototal = montototal;
	}

	public List<DetalleVentas> getItems() {
		return items;
	}

	public void setItems(List<DetalleVentas> items) {
		this.items = items;
	}


}
