package com.cajapaita.pe.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "detalleventas")
public class DetalleVentas {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer cod_detalle;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "cod_venta", nullable = false)
	private Venta venta;
	
	@ManyToOne
	@JoinColumn(name="cod_producto",nullable=false)
	private Producto producto;
	
	private Integer cantidad;
	private Double precio;
	public Integer getCod_detalle() {
		return cod_detalle;
	}
	public void setCod_detalle(Integer cod_detalle) {
		this.cod_detalle = cod_detalle;
	}
	public Venta getVenta() {
		return venta;
	}
	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	
}
