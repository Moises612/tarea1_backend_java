package com.cajapaita.pe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cajapaita.pe.dao.IPersonaDao;
import com.cajapaita.pe.model.Persona;
import com.cajapaita.pe.service.IPersonaService;

@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	private IPersonaDao personaDao;

	@Override
	public Persona registrar(Persona t) {

		return personaDao.save(t);
	}

	@Override
	public Persona modificar(Persona t) {
		return personaDao.save(t);
	}

	@Override
	public void eliminar(int id) {
		personaDao.delete(id);
	}

	@Override
	public Persona listarid(int id) {
		return personaDao.findOne(id);
	}

	@Override
	public List<Persona> listar() {
		return personaDao.findAll();
	}

}
