package com.cajapaita.pe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cajapaita.pe.dao.IVentaDao;
import com.cajapaita.pe.model.DetalleVentas;
import com.cajapaita.pe.model.Pudauob;
import com.cajapaita.pe.model.Venta;
import com.cajapaita.pe.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private IVentaDao ventaDao;
	@Override
	public Venta registrar(Venta t) {
		
		for(DetalleVentas detalle: t.getItems()) {
			detalle.setVenta(t);
		}
		
		return ventaDao.save(t);
	}

	@Override
	public Venta modificar(Venta t) {
		return ventaDao.save(t);
	}

	@Override
	public void eliminar(int id) {
		ventaDao.delete(id);
	}

	@Override
	public Venta listarid(int id) {
		return ventaDao.findOne(id);
	}

	@Override
	public List<Venta> listar() {
		return ventaDao.findAll();
	}

}
