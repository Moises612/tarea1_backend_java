package com.cajapaita.pe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cajapaita.pe.dao.IProductoDao;
import com.cajapaita.pe.model.Producto;
import com.cajapaita.pe.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoDao productoDao;

	@Override
	public Producto registrar(Producto t) {

		return productoDao.save(t);
	}

	@Override
	public Producto modificar(Producto t) {

		return productoDao.save(t);
	}

	@Override
	public void eliminar(int id) {
		productoDao.delete(id);

	}

	@Override
	public Producto listarid(int id) {

		return productoDao.findOne(id);
	}

	@Override
	public List<Producto> listar() {
		return productoDao.findAll();
	}

}
